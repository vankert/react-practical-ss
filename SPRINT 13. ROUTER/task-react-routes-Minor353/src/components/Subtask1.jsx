import { useParams } from 'react-router-dom'

export default function Subtask1() {
    const params = useParams();

    return (
        <div>
            Subtask1, parameter: {params.id}
        </div>
    )
}