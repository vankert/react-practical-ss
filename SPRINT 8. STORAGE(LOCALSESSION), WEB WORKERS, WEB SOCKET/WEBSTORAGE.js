// Add two pairs to localStorage:

// key: "login", value: "Tom12!"
// key: "token", value: "QhuR56Rw"

localStorage.setItem('login', 'Tom12!');
localStorage.setItem('token', 'QhuR56Rw');

/***********************************************************************************************************/

// Save object { name: "Tom", age: 25 } to localStorage with key: "user"

const obj = { name: 'Tom', age: 25 };
localStorage.setItem('user', JSON.stringify(obj));

/***********************************************************************************************************/

// Remove pair from localStorage with key: "name"

localStorage.removeItem('name');

/************************************************************************************************************/

// Clear all data in localStorage

localStorage.clear();
