import { configureStore, combineReducers } from "@reduxjs/toolkit";
import uiSlice from './ui_slice';
import cartSlice from './cart_slice';

const rootReducer = combineReducers({
    ui: uiSlice.reducer,
    cart: cartSlice.reducer
  });

const store = configureStore({
    reducer: rootReducer
});

export default store;
