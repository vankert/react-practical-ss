const express = require('express');
const app = express();

const interval = process.env.INTERVAL || 1000; 
const timeout = process.env.TIMEOUT || 5000; 

app.get('/', (req, res) => {
  const intervalId = setInterval(() => {
    console.log(new Date().toUTCString());
  }, interval);

  setTimeout(() => {
    clearInterval(intervalId);
    const currentDate = new Date().toUTCString();
    console.log(`Stopped interval at ${currentDate}`);
    res.send(currentDate);
  }, timeout);
});

const port = process.env.PORT || 3000;
app.listen(port, () => {
  console.log(`Server started on port ${port}`);
});
