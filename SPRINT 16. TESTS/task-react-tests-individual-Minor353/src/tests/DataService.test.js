import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import getGitHubUser from '../services/DataService';

describe('DataService', () => {
  it('fetches data from GitHub API', async () => {
    const mock = new MockAdapter(axios);
    const data = { response: true };
    mock.onGet('https://api.github.com/users/test').reply(200, data);

    const response = await getGitHubUser('test');
    
    expect(response.data).toEqual(data);
  });
});