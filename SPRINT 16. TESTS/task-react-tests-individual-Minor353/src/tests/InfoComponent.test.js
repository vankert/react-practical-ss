import { render, screen, waitFor } from '@testing-library/react';
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import Info from '../components/Info';

describe('Info Component', () => {
  it('fetches and displays data', async () => {
    const mock = new MockAdapter(axios);
    const data = { login: 'test', id: 1 };
    mock.onGet('https://api.github.com/users/test').reply(200, data);

    render(<Info user="test" />);

    await waitFor(() => {
      expect(screen.getByText(/login: test/i)).toBeInTheDocument();
    });

    expect(screen.getByText(/id: 1/i)).toBeInTheDocument();
  });

  it('displays error message on request error', async () => {
    const mock = new MockAdapter(axios);
    mock.onGet('https://api.github.com/users/test').reply(500);

    render(<Info user="test" />);

    await waitFor(() => {
      expect(screen.getByText(/request error/i)).toBeInTheDocument();
    });
  });
});
