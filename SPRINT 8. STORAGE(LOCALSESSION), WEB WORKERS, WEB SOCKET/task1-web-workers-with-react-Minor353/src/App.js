import { useState } from "react";
import "./App.css";

//Note: please, do dot change placeholder and data-testid attributes

function App() {
  const [result, setResult] = useState("");
  const [inputData, setInputData] = useState("");
  const [worker, setWorker] = useState(null);

  const fieldChangeHandle = (evt) => {
    const { value } = evt.target;
    setInputData(value);
  };

  const handleClick = () => {
    if (worker !== null) {
      worker.terminate();
    }

    setResult("Calculating...");

    const newWorker = new Worker("worker.js");

    newWorker.onmessage = (event) => {
      setResult(`Result: ${event.data}`);
      setWorker(null);
    };

    newWorker.postMessage({ data: inputData });

    setWorker(newWorker);
  };

  return (
    <div className="app">
      <h1>Fibonacci 🌀</h1>
      <input
        onChange={fieldChangeHandle}
        type="number"
        placeholder="Insert a number"
        value={inputData}
      />
      <button onClick={handleClick}>Calculate</button>
      <div className="result" data-testid="result">
        {result}
      </div>
    </div>
  );
}

export default App;
