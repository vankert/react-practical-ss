import React, { useCallback } from "react";
import stylesCenter from "./index.module.css";
import ContactItem from "./ContactItem";
import { v4 as uuidv4 } from 'uuid';


const Contacts = ({contactInfo, setContactInfo}) => {  

  const handleAddDetails = () => {
    setContactInfo([
      ...contactInfo,
      { optionSelected: "", details: "", key: uuidv4() },
    ]);
  };

  const handleChangeDetailsAndOptions = useCallback(
    (index, event) => {      
      setContactInfo(
      previousvalues => {
        previousvalues[index][event.target.name] = event.target.value;
        return [...previousvalues]          
      });
    },
    [setContactInfo]
  );

  const handleRemoveField = useCallback(
    (index) => {    
      setContactInfo(prev => {        
        prev.splice(index, 1); return[...prev]
    });
    },
    [setContactInfo]
  );

  return (
    <>
      <div className={stylesCenter.channels}>
        {contactInfo.map((element, index) => (
          <ContactItem
            optionSelected={element.optionSelected}
            details={element.details}
            key={element.key}            
            index={index}
            onChange={handleChangeDetailsAndOptions}
            setInputFieldsAndOptions={setContactInfo}
            handleRemoveField={handleRemoveField}
          />
        ))}
      </div>
      <div>
        <button
          className={stylesCenter.addButton}
          onClick={handleAddDetails}
          data-testid="add-button"
        >
          <img src="plus.svg" alt="plus logo" />
          <span className={stylesCenter.addButtonText}>
            Додати канал зв'язку
          </span>
        </button>
      </div>
    </>
  );
};

export default Contacts;