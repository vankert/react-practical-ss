import { useLocation } from 'react-router-dom';


export default function Subtask2() {
    const { search } = useLocation();
    const params = new URLSearchParams(search);
    const query = Array.from(params.entries()).map(([key, value]) => `${key}=${value}`).join(', ');
    return (
        <div>
            Subtask2, query parameters: {query}
        </div>
    )
}