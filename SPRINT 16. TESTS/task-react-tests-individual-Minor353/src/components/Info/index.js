import { Component } from "react";
import getGitHubUser from "../../services/DataService";

class Info extends Component {
  _isMounted = false;

  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    this._isMounted = true;

    getGitHubUser(this.props.user)
      .then((response) => {
        if (this._isMounted) {
          this.setState(response.data);
        }
      })
      .catch((error) => {
        if (this._isMounted) {
          this.setState({ error: "request error" });
        }
      });
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  render() {
    return (
      <div>
        <h3>GitHub User Info</h3>
        <ul>
          {Object.keys(this.state).map((i) => (
            <li key={i}>
              {i}: {this.state[i]}
            </li>
          ))}
        </ul>
      </div>
    );
  }
}

export default Info;
