import React, { Component } from "react";

export default class Task2 extends Component {
  constructor(props) {
    super(props);

    this.state = {
      list: [
        {
          id: 1,
          text: "hidden text 1",
        },
        {
          id: 2,
          text: "hidden text 2",
        },
        {
          id: 3,
          text: "hidden text 3",
        },
      ],
      text: "",
    };
  }

  hoverEnterHandler = (event) => {
    let currentText = this.state.list.find(
      (item) => item.id === Number(event.target.id)
    );
    this.setState({ text: currentText.text });
  };

  hoverOverHandler = () => {
    this.setState({ text: "" });
  };

  render() {
    return (
      <>
        <ul>
          {this.state.list.map((item) => {
            return (
              <li
                onMouseEnter={this.hoverEnterHandler}
                onMouseLeave={this.hoverOverHandler}
                key={item.id}
                id={item.id}
                className="element"
              >
                id - {item.id}
              </li>
            );
          })}
        </ul>
        <span data-testid="text">{this.state.text}</span>
      </>
    );
  }
}
