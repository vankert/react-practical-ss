import DataItem from './DataItem';
import { ErrorBoundary } from "react-error-boundary";
import ErrorFallback from './ErrorFallback';


function DataList() {  

    

    const count = [1, 2, 3];
    return (
        count.map(i => {
          return  (
            <ErrorBoundary FallbackComponent={ErrorFallback}> 
                <DataItem key={i} />
            </ErrorBoundary>
          )
        })
    );
}

export default DataList;
