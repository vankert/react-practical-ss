import React from 'react';
import { Outlet} from 'react-router-dom';
import { useNavigate } from 'react-router-dom';
import { useState } from 'react';

import './App.css';

export default function App() {
  const [inputValue, setInputValue] = useState('');
  const [isChecked, setIsChecked] = useState(false);
  const navigate = useNavigate();

  const handleInputChange = (event) => {
    setInputValue(event.target.value);
  };

  const handleClick = (event) => {
    event.preventDefault();
    (inputValue !== '' && inputValue%2 !== 0) ? navigate('/subtask3') : navigate('/')
  }

  const handleCheckboxChange = (event) => {
    const isChecked = event.target.checked;
    setIsChecked(isChecked);
    isChecked ? navigate('/subtask4') : navigate('/');
  }

  return (
    <div className="App">
      <h1>React Marathon</h1>
      <h2>The topic 'Routes'</h2>
      <a href='/' onClick={handleClick}>Show protected information if</a>
      <span>&nbsp;</span>
      <input size="5" value={inputValue} onChange={handleInputChange}></input> is odd
      <div className="mainClass">
        Go to the component programmatically, by checking the box:{" "}
        <input 
          type="checkbox"
          checked={isChecked}
          onChange={handleCheckboxChange}></input>
      </div>
      <Outlet/>
    </div>
  );
}
