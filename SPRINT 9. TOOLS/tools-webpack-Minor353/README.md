[![Review Assignment Due Date](https://classroom.github.com/assets/deadline-readme-button-24ddc0f5d75046c5622901739e7c5dd533143b0c8e959d652212380cedb1ea36.svg)](https://classroom.github.com/a/tqfEJkyd)
# React online marathon

## The tasks of the topic "Webpack"

There is the file <code>webpack.config.js</code>

Configure it to specify:

1. Define the path <code>./src/main.js</code> as an entry property in your webpack configuration
2. Your configuration should output a single <code>build.js</code> file into the <code>pub</code> directory
