[![Review Assignment Due Date](https://classroom.github.com/assets/deadline-readme-button-24ddc0f5d75046c5622901739e7c5dd533143b0c8e959d652212380cedb1ea36.svg)](https://classroom.github.com/a/9Do66Qks)
# React online marathon

## The task of the topic "Hooks"

There is the functional component `App` which renders one element `div` with text `React Marathon` in it

Using `hooks` write code to provide next functionality: if you click mouse on this `div` element text in it will be converted to lowercase

### Example
![](example.gif)
