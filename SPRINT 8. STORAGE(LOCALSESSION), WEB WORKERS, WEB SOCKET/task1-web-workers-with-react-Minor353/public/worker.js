function fibonacci(num) {
  if (num <= 1) return num;
  let fibPrev = 1;
  let fib = 1;
  for (let i = 2; i < num; i++) {
    const temp = fib;
    fib += fibPrev;
    fibPrev = temp;
  }
  return fib;
}

onmessage = (event) => {
  const inputValue = event.data.data;
  const result = fibonacci(inputValue);
  postMessage(+result);
};
