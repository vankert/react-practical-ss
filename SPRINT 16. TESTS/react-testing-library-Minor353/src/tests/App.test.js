import {render, screen} from "@testing-library/react";
import App from '../components/App';
import userEvent from '@testing-library/user-event';

describe("App", ()=> {
    test("image is displayed when the first tab is selected", ()=> {
        render(<App/>);
        const pictureButton = screen.getByRole("button", {name: "Picture"});
        userEvent.click(pictureButton);
        const picture = screen.getByRole('img');

        expect(pictureButton).toHaveClass('active');
        expect(picture).toBeInTheDocument();

    })

    test("Calculation component is displayed when the second tab is selected", ()=> {
        render(<App/>);
        const calculationButton = screen.getByRole("button", {name: "Calculations"});
        userEvent.click(calculationButton);
        const calculationTitle = screen.getByRole('heading', {level: 5, name: "Result"});

        expect(calculationButton).toHaveClass('active');
        expect(calculationTitle).toBeInTheDocument();

    })

    test("ButtonGroup component is displayed when the third tab is selected", ()=> {
        render(<App/>);
        const groupButton = screen.getByRole("button", {name: "Group"});
        userEvent.click(groupButton);
        const groupTitle = screen.getByRole('heading', {level: 5, name: "Align"});

        expect(groupButton).toHaveClass('active');
        expect(groupTitle).toBeInTheDocument();

    })

    test("components ButtonGroup and Calculation are not displayed if they do not correspond to the currenct Image active tab", ()=> {
        render(<App/>);
        const pictureButton = screen.getByRole("button", {name: "Picture"});
        userEvent.click(pictureButton);
        const calculationTitle = screen.queryByRole('heading', {level: 5, name: "Result"});
        const groupTitle = screen.queryByRole('heading', {level: 5, name: "Align"});

        expect(calculationTitle).not.toBeInTheDocument();
        expect(groupTitle).not.toBeInTheDocument();

        

    })
})