import React from 'react';
import './App.css';

const list = [
  'Animals',
  'Anime',
  'Anti-Malware',
  'Art Design',
  'Books',
  'Business',
  'Calendar',
  'Cloud Storage',
  'File Sharing',
  'Animals',
  'Continuous Integration',
  'Cryptocurrency',
];

export default function App() {
  let index = 0;
  return (
    <div>
      Some List:
      <ul>
        {list.map((item) => {
          index += 1;
          return <li key={index}>{item}</li>;
        })}
      </ul>
    </div>
  );
}
