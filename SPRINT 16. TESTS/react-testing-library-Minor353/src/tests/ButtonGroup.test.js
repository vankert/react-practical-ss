import {render, screen} from "@testing-library/react";
import ButtonGroup from '../components/ButtonGroup';
import userEvent from '@testing-library/user-event';

describe("ButtonGroup", ()=> {
    test("check text alignment in a paragraph to the left", ()=> {
        render(<ButtonGroup/>);
        const text = screen.getByTestId("text");
        const leftButton = screen.getByLabelText("left");

        userEvent.click(leftButton);

        expect(text).toHaveAttribute('align', 'left')

    })

    test("check text alignment in a paragraph to the center", ()=> {
        render(<ButtonGroup/>);
        const text = screen.getByTestId("text");
        const centerButton = screen.getByLabelText("center");

        userEvent.click(centerButton);

        expect(text).toHaveAttribute('align', 'center')

    })

    test("check text alignment in a paragraph to the right", ()=> {
        render(<ButtonGroup/>);
        const text = screen.getByTestId("text");
        const rightButton = screen.getByLabelText("right");

        userEvent.click(rightButton);

        expect(text).toHaveAttribute('align', 'right')

    })

})