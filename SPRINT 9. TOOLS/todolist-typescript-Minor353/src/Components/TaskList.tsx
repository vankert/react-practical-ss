import TaskItem from "./TaskItem";
import { useRef, useState } from "react";
import { Task } from "./TaskItem";

  
  type TaskListProps = {
    tasks?: Task[];
  }

const TaskList = ({ tasks }: TaskListProps) => {
    if(tasks){
        localStorage.setItem("tasks", JSON.stringify(tasks));
    }
    const tasksFromLocalStorage = JSON.parse(localStorage.getItem("tasks") || "[]");
    const [componentList, setComponentList] = useState<Task[]>(tasks || tasksFromLocalStorage || []);
   

    const inputRef = useRef<HTMLInputElement | null>(null);

    const handleRemoveTask = (id: number) => {
        const newComponentList =  componentList.filter((component) => {
            return component.id !== id;
          });
        setComponentList(newComponentList);
        localStorage.setItem("tasks", JSON.stringify(newComponentList));
    }
 
    const handleToggleTask = (id: number) => {
        const newComponentList = componentList.map((component) => {
            return (
                component.id === id ? { ...component, completed: !component.completed } : component
            )
        }  
    )
        setComponentList(newComponentList);
        localStorage.setItem("tasks", JSON.stringify(newComponentList));
    }

    const handleAddTask: React.MouseEventHandler<HTMLButtonElement> = () => {
        const inputVal = inputRef.current?.value;
        const newComponentList = [...componentList, 
            {
                id: componentList.length + 1, 
                text: inputVal, 
                completed: false
            }
    ];
        setComponentList(newComponentList);
        localStorage.setItem("tasks", JSON.stringify(newComponentList));
        if (inputRef.current) {
            inputRef.current.value = '';
          }
    }


   
 
  return (
    <div className="task-list">
      <h2 className="task-list-title">Task List</h2>
      <div className="add-task-form">
        <input className="add-task-input" type="text" ref={inputRef} />
        <button onClick={handleAddTask} className="add-task-button" type="button">Add</button>
      </div>
      <ul>
        {
            componentList.length ? componentList.map((component) => {
                return (
                    <TaskItem
                        key={component.id}
                        task={component}
                        onRemove={(id) => handleRemoveTask(id)}
                        onToggle={(id) => handleToggleTask(id)}
                    />
                )
            }) : <div className="task-list-empty">No tasks to display</div>
        }
      </ul>

    </div>
  );
};

export default TaskList;
