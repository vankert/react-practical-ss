import React from 'react'
import { ContactContext } from './App'
import { useContext } from 'react';

function ChannelStatistics() {
  const contactInfo = useContext(ContactContext);
  const lastChannelName = contactInfo[contactInfo.length - 1].optionSelected;

  return (
    <p data-testid="statistics">
      Count of channels: {contactInfo.length} <br />
      {lastChannelName && `your last channel is: ${lastChannelName}`}
    </p>
  )
}

export default ChannelStatistics