import React, { useState } from "react";
import './App.css';
import Contacts from './contacts';
import Logo from "./Logo";
import { v4 as uuidv4 } from 'uuid';

export const ContactContext = React.createContext();


function App() {

  const [contactInfo, setContactInfo] = useState(() => [
    { optionSelected: "", details: "" , key: uuidv4()},
  ]);  

  return (
    <div className="grid-container">
      <div>
        <Contacts
          contactInfo={contactInfo}
          setContactInfo={setContactInfo}          
        />
      </div>
      <div>
        <ContactContext.Provider value={contactInfo}>
          <Logo />
        </ContactContext.Provider>
      </div>
    </div>
  );
}

export default App;
