import { useState } from "react";
import "./App.css";

export default function App() {
  const [text, setTextToLowerCase] = useState("React Marathon");

  function handleTextToLowerCase() {
    setTextToLowerCase(text.toLowerCase());
  }

  return <div onClick={handleTextToLowerCase}> {text} </div>;
}
