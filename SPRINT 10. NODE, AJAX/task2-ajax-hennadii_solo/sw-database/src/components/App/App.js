import { createApi } from '../../services/sw-service';
import { useState, useEffect } from "react";
import './App.css'

function App() {
  const api = createApi();

  const imgTypeLInks = {
    'characters': 'https://starwars-visualguide.com/assets/img/characters/',
    'planets': 'https://starwars-visualguide.com/assets/img/planets/',
    'starships': 'https://starwars-visualguide.com/assets/img/starships/'
  }
  
  const [currentData, setCurrentData] = useState({
    id: 1,
    data: {}
  });

  const [onLoad, setOnLoad] = useState(true);

  const [isLoadError, setisLoadError] = useState(false);

  const [isImgError, setIsImgError] = useState(false);

  const [dataType, setDataType] = useState('characters');

  const loadCharacters = () => {
    api.getPerson(currentData.id)
    .then(response => {
      setCurrentData(prevData => ({
        ...prevData,
        data: {
          name: response.data.name,
          gender: response.data.gender,
          birth_year: response.data.birth_year,
          eye_color: response.data.eye_color
        }
      }));

      setOnLoad(false);
    })
    .catch(error => {
      if (error.response && error.response.status === 404) {
        setisLoadError(true);
        console.log('Resource not found!');
      } else {
        console.log('Error:', error);
      }
    });
  }

  const loadPlanets = () => {
    api.getPlanet(currentData.id)
    .then(response => {
      setCurrentData(prevData => ({
        ...prevData,
        data: {
          name: response.data.name,
          climate: response.data.climate,
          gravity: response.data.gravity,
          population: response.data.population
        }
      }));

      setOnLoad(false);
    })
    .catch(error => {
      if (error.response && error.response.status === 404) {
        setisLoadError(true);
      } else {
        console.log('Error:', error);
      }
    });
  }

  const loadStarships = () => {
    api.getStarships(currentData.id)
    .then(response => {
      setCurrentData(prevData => ({
        ...prevData,
        data: {
          name: response.data.name,
          crew: response.data.crew,
          passengers: response.data.passengers,
          starship_class: response.data.starship_class
        }
      }));

      setOnLoad(false);
    })
    .catch(error => {
      if (error.response && error.response.status === 404) {
        setisLoadError(true);
      } else {
        console.log('Error:', error);
      }
    });
  }


  useEffect(()=> {
    setOnLoad(true);
    (dataType === 'characters') 
    ? loadCharacters() 
    : (dataType === 'planets') 
    ? loadPlanets() 
    : loadStarships();
  },[currentData.id, dataType]);

  const handleShowNextDataClick = () => {
    setIsImgError(false);
    setCurrentData({
      ...currentData,
      id: currentData.id + 1,
    })
  }

  const handleChangeDataTypeClick = (event) => {
    const type = event.target.dataset.type;
    setDataType(type);
  }

  const handleErrorImg = () => {
    setIsImgError(true);
  }

  return (
    <div className="App">
      <header className='header'>
        <ul>
          <li onClick={handleChangeDataTypeClick} data-type='characters' className={(dataType === 'characters') ? 'active' : ''}>People</li>
          <li onClick={handleChangeDataTypeClick} data-type='planets' className={(dataType === 'planets') ? 'active' : ''}>Planets</li>
          <li onClick={handleChangeDataTypeClick} data-type='starships' className={(dataType === 'starships') ? 'active' : ''}>Starships</li>
        </ul>
      </header>
        <div className='data-block'>
          <button onClick={handleShowNextDataClick} className='data-block_button' type='button'>NEXT</button>

          <div className='data-block_wrapp'>
            {
              onLoad ? <div className='data-block_loader'>{isLoadError ? 'Data not found' : 'Loading...'}</div> : null
            }
            
            <div className='data-block_img'>
                {isImgError ? <div className='data-block_img-not-found'>Image not found</div> : null}
                <img onError={handleErrorImg}  src={`${imgTypeLInks[dataType]}${currentData.id}.jpg`} alt="img" />
            </div>
            <h3 className='data-block_name'>{currentData.data.name}</h3>
            <ul className='data-block_list'>
              {Object.keys(currentData.data).slice(1).map((key) => (
                <li className='data-block_list-item' key={key}>
                  <span>{key}</span>
                  <span>{currentData.data[key]}</span>
                </li>
              ))}
            </ul>
          </div>
        </div>
    </div>
  );
}

export default App;
