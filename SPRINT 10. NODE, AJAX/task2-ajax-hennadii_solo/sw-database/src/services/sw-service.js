import axios from 'axios';

const SWAPI_BASE_URL = 'https://swapi.dev/api/';


export const createApi = () => {
  const api = axios.create({
    baseURL: SWAPI_BASE_URL,
  });

  api.getPerson = (id) => api.get(`/people/${id}`);

  api.getPlanet = (id) => api.get(`/planets/${id}`);

  api.getStarships = (id) => api.get(`/starships/${id}`);

  return api;
};