import React from "react";

import myStyles from "./input.module.css";

function Input() {
  return (
    <input type="text" className={myStyles.active} placeholder="your text" />
  );
}

export default Input;
