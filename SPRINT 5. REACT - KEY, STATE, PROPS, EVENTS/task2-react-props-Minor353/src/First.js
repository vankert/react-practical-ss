import React from 'react';

export default function First({ list }) {
  return (
    <ul>
      <li>{list[0]}</li>
    </ul>
  );
}
