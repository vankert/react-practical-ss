export type Task = {
    id?: number;
    text?: string;
    completed?: boolean;
  }

  type TaskItemProps = {
    task: Task;
    onRemove: (id: number) => void;
    onToggle: (id: number) => void;
  }


const TaskItem = ({task, onRemove, onToggle}: TaskItemProps) => {
    const textDecoration = task.completed ? "line-through" : "none";
    const isChecked = task.completed;

    const handleRemoveClick = () => {
        if (task.id) {
            onRemove(task.id);
        }
      };

    const handleToggleClick = () => {
        if (task.id) {
            onToggle(task.id);
        }
    };

  return (
    <li className="task-item">
        <input onClick={handleToggleClick} className="task-item-checkbox" defaultChecked={isChecked} type="checkbox" name="done" />
        <span className="task-item-text" style={{ textDecoration }}>{task.text}</span>
        <button onClick={handleRemoveClick} className="task-item-remove-button" type="button">Remove</button>
    </li>
  );
};

export default TaskItem;