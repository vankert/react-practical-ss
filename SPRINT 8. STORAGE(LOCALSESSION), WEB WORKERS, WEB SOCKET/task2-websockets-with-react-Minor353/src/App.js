import { useState } from "react";
import "./App.css";

// Note: please, do not change the next things:
// - name of App prop,
// - placeholder and aria-label values
// - text on the button

function App({ ws }) {
  const [formData, setFormData] = useState({
    nickname: "",
    message: "",
  });

  const [receivedText, setReceivedText] = useState([]);

  const fieldChangeHandle = (evt) => {
    const { name, value } = evt.target;
    setFormData({ ...formData, [name]: value });
  };

  const sendMsgHandle = () => {
    const dataToSend = `${formData.nickname}: ${formData.message}`;
    ws.send(dataToSend);
  };

  ws.onmessage = function (event) {
    setReceivedText([...receivedText, event.data]);
  };
  return (
    <div className="App">
      <h1>Web Sockets</h1>
      <div>
        <textarea
          rows="30"
          cols="60"
          readOnly
          aria-label="chat"
          value={receivedText.join("\n")}
        />
      </div>
      <input
        onChange={fieldChangeHandle}
        placeholder="Your nickname"
        size="11"
        value={formData.nickname}
        name="nickname"
      />
      <input
        onChange={fieldChangeHandle}
        placeholder="Type your message"
        size="40"
        value={formData.message}
        name="message"
      />
      <button onClick={sendMsgHandle}>Send</button>
    </div>
  );
}

export default App;
