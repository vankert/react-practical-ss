import { useState, useEffect } from "react";
import "./App.css";

export default function App() {
  const [appData, setAppData] = useState("");

  useEffect(() => {
    const data = localStorage.getItem("appData");
    setAppData(data);
  }, []);

  return (
    <div>
      React Marathon, appData: <input size="5" defaultValue={appData}></input>
    </div>
  );
}
