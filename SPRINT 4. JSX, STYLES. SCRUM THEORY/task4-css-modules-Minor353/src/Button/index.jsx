import React from "react";

import myStyles from "./button.module.css";

function Button() {
  return <button className={myStyles.active}>Click Me</button>;
}

export default Button;
