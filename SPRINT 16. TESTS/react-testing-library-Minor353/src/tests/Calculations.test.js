import {render, screen} from "@testing-library/react";
import Calculations from '../components/Calculations';
import userEvent from '@testing-library/user-event';

describe("Calculations", ()=> {
    test("Number addition operation is performed correctly", ()=> {
        render(<Calculations/>);
        const resultTitle = screen.getByRole("heading", {level: 4, name: 0})
        const operationButton = screen.getByRole("button", { name: "operation" });
        const incButton = screen.getByTestId("dropdown-item-dec")
        const firstInputElement = screen.getByLabelText('first number');
        const secondInputElement = screen.getByLabelText('second number');
        const evaluateButton = screen.getByRole("button", {name: "Evaluate"});

        userEvent.clear(firstInputElement);
        userEvent.type(firstInputElement, "5");
        userEvent.clear(secondInputElement);
        userEvent.type(secondInputElement, "5");
        userEvent.click(operationButton);
        userEvent.click(incButton);
        userEvent.click(evaluateButton);

        expect(resultTitle).toHaveTextContent("10");

    })

    test("Number subtraction operation is performed correctly", ()=> {
        render(<Calculations/>);
        const resultTitle = screen.getByRole("heading", {level: 4, name: 0})
        const operationButton = screen.getByRole("button", { name: "operation" });
        const decButton = screen.getByText("-");
        const firstInputElement = screen.getByLabelText('first number');
        const secondInputElement = screen.getByLabelText('second number');
        const evaluateButton = screen.getByRole("button", {name: "Evaluate"});


        userEvent.clear(firstInputElement);
        userEvent.type(firstInputElement, "15");
        userEvent.clear(secondInputElement);
        userEvent.type(secondInputElement, "3");
        userEvent.click(operationButton);
        userEvent.click(decButton);
        userEvent.click(evaluateButton);

        expect(resultTitle).toHaveTextContent("12");
    })

})