function MyLabel() {
  return (
    <label htmlFor="inp-num" data-testid="element-label">
      Count:
    </label>
  );
}

export default MyLabel;
