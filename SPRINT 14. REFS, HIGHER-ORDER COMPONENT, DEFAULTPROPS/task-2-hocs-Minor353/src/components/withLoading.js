import React, { useState, useEffect } from 'react';

const withLoading = (WrappedComponent) => {
  return function WithLoadingComponent({ fetchMethod, params, ...props }) {
    const [data, setData] = useState(null);
    const [isLoading, setIsLoading] = useState(true);

    useEffect(() => {
      setIsLoading(true);
      fetchMethod(params)
        .then(res => {
          setData(res);
          setIsLoading(false);
        });
    }, [params, fetchMethod]);

    if (isLoading) {
      return <p className='center'>Loading...</p>
    } else {
      return <WrappedComponent {...props} data={data} />;
    }
  };
};

export default withLoading;
