import React, { Component } from "react";

const listStyle = {
  display: "flex",
  flexWrap: "wrap",
  marginTop: "-5px",
  marginLeft: "-5px",
};

const listBlockStyle = {
  border: "1px solid #000",
  marginTop: "10px",
  marginLeft: "10px",
  minWidth: "300px",
};

export default class Task1 extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: true,
      list: null,
    };
  }

  componentDidMount() {
    fetch("http://localhost:3000/list")
      .then((response) => response.json())
      .then((list) => {
        this.setState({ isLoading: false, list });
      })
      .catch((error) => {
        console.error("Error fetching data:", error);
      });
  }

  render() {
    const { isLoading, list } = this.state;
    if (isLoading) {
      return <div style={listBlockStyle}>Loading...</div>;
    }

    return (
      <div style={listStyle}>
        {list.map((item) => (
          <div style={listBlockStyle} key={item.id}>
            id - {item.id}name - {item.name}note - {item.note}
          </div>
        ))}
      </div>
    );
  }
}
