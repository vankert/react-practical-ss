import classes from './CartButton.module.css';
import {uiActions} from '../../store/ui_slice';
import { useSelector ,useDispatch } from 'react-redux';

const CartButton = (props) => {
  const dispatch = useDispatch();
  const cartCount = useSelector((state)=>state.cart.totalQuantity);
  return (
    <button onClick={()=>dispatch(uiActions.toggle())} className={classes.button}>
      <span>My Cart</span>
      <span className={classes.badge}>{cartCount}</span>
    </button>
  );
};

export default CartButton;
