import React from 'react';
import './App.css';
import First from './First';

const list = [
  'Animals',
  'Anime',
  'Anti-Malware',
  'Art Design',
  'Books',
  'Business',
  'Calendar',
  'Cloud Storage',
  'File Sharing',
  'Animals',
  'Continuous Integration',
  'Cryptocurrency',
];

const newList = list.map((item) => {
  return item.toLowerCase();
});

export default function App() {
  return (
    <div>
      Some data:
      <First list={newList} />
    </div>
  );
}
