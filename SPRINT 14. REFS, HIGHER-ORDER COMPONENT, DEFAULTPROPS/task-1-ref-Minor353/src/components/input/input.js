import React, { Component } from 'react';

import './input.css';


export default class Input extends Component{
    constructor(props) {
        super (props);
        this.operatorRef = React.createRef();
        this.operatorNameRef = React.createRef();
        this.phoneRef = React.createRef();
        this.phoneCheckRef = React.createRef();
        this.state = {
            operatorValue: '',
            numberValue: ''
          };
        this.operators = {
            'Kyivstar': [67, 68, 96, 97, 98],
            'Vodafone': [50, 66, 95, 99],
            'Lifecell': [63, 73, 93],
            '3mob': [91],
            'People.net': [92],
            'intertelecom': [89, 94]
        };
    }

    componentDidMount() {
        this.operatorRef.current.focus();
    }


    inputHandler = () => {
        const operatorInput = this.operatorRef.current.value;
        const operatorOnlyDigits = operatorInput.replace(/\D/g, '').slice(0, 2);

        const phoneInput = this.phoneRef.current.value;
        const phoneOnlyDigits = phoneInput.replace(/\D/g, '').slice(0, 7);

        this.setState({ operatorValue: operatorOnlyDigits, numberValue: phoneOnlyDigits }, () => {
            if (this.state.operatorValue.length === 2) {
                const operators = this.operators;
                const matchedOperator = Object.keys(operators).find((operator) =>
                    operators[operator].includes(parseInt(this.state.operatorValue))
                );

                this.operatorNameRef.current.textContent = matchedOperator || 'Unknown';

                this.phoneRef.current.focus();
            } else {
                this.operatorNameRef.current.textContent = '';
            }

            if (this.state.numberValue.length === 7 && this.state.operatorValue.length === 2) {
                this.phoneCheckRef.current.textContent = this.phoneCheckRef.current.textContent.replace(/-/g, '✔️');
            } else {
                this.phoneCheckRef.current.textContent = this.phoneCheckRef.current.textContent.replace(/✔️/g, '-');
            }
            });
    }

     render(){
        return <div>
            <span data-testid="operator-name" ref={this.operatorNameRef}></span>
            <span>+38 0</span>
            <input 
                type="text" 
                data-testid="operator-input"
                ref={this.operatorRef}
                value={this.state.operatorValue}
                onInput={this.inputHandler}
                />
            <span data-testid="check-icon" ref={this.phoneCheckRef}> - </span>
            <input type="text" 
                data-testid="phone-input"
                ref={this.phoneRef}
                value={this.state.numberValue}
                onInput={this.inputHandler}
                 />
        </div>;
    }
}
