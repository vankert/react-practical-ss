import { BrowserRouter, Routes, Route } from 'react-router-dom';
import { StrictMode } from 'react';
import { createRoot } from 'react-dom/client';

import App from './App';
import Subtask1 from './components/Subtask1';
import Subtask2 from './components/Subtask2';
import Subtask3 from './components/Subtask3';
import Subtask4 from './components/Subtask4';

const rootElement = document.getElementById('root');
const root = createRoot(rootElement);

root.render(
  <StrictMode>
    <BrowserRouter>
      <Routes>
        <Route path='/' element={<App />}>
          <Route path='/subtask1/:id' element={<Subtask1/>}/> 
          <Route path='/subtask2' element={<Subtask2 />} /> 
          <Route path='/subtask3' element={<Subtask3 />} /> 
          <Route path='/subtask4' element={<Subtask4 />} /> 
        </Route>
      </Routes>
    </BrowserRouter>
  </StrictMode>
);
