
import { render, screen } from '@testing-library/react';
import App from '../components/App';

describe('App Component', () => {
  it('renders without crashing', () => {
    render(<App />);

    expect(screen.getByText(/Hello World!/i)).toBeInTheDocument();
    const infoElements = screen.getAllByText(/GitHub User Info/i);
    expect(infoElements).toHaveLength(2);
  });
});
