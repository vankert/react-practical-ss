const WebSocket = require("ws");
const wss = new WebSocket.Server({ port: 8082 });

const clients = [];

function sendForAll(message, sender) {
  clients.forEach((client) => {
    if (client.readyState === WebSocket.OPEN) {
      client.send(`${sender}: ${message}`);
    }
  });
}

wss.on("connection", (conection) => {
  clients.push(conection);

  conection.on("message", (message) => {
    sendForAll(message, "Sender");
  });

  conection.on("close", () => {
    clients.splice(clients.indexOf(conection), 1);
  });
});

// Do not remove this export. wss should be the name of you WebSocket Server instance
module.exports = wss;
