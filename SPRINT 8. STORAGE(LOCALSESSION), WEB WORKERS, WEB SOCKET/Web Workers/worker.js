// Слушаем сообщения от основного потока
onmessage = function (event) {
  if (event.data === 'start') {
    // Выполняем долгую операцию
    let result = 0;
    for (let i = 0; i < 1000000000; i++) {
      result += i;
    }

    // Отправляем ответ обратно в основной поток
    postMessage('Result: ' + result);
  }
};

// В этом примере мы создаем новый Web Worker и отправляем ему сообщение "start".
// Web Worker начинает выполнение долгой операции - подсчет суммы от 0 до 1 миллиарда.
// После завершения операции Web Worker отправляет ответ обратно в основной поток,
// который выводит результат на страницу.

// Когда вы откроете эту страницу в браузере, вы увидите, что интерфейс продолжает работать без задержек,
// даже когда Web Worker выполняет долгую операцию.
/*******************************************************************************/

// Веб-воркеры поддерживают ряд других методов и событий, помимо postMessage и onmessage.

// Методы:

// terminate(): прерывает работу веб-воркера;
// importScripts(): загружает дополнительные скрипты в контекст воркера;
// addEventListener(): позволяет подписаться на события воркера, такие как error и messageerror.
// События:

// onerror: срабатывает при возникновении ошибки в воркере;
// onmessageerror: срабатывает при возникновении ошибки в сообщении, отправленном в воркер.

// Вот пример использования метода addEventListener():
// Родительский скрипт
const worker = new Worker('worker.js');

worker.addEventListener('message', (event) => {
  console.log(`Родительский скрипт получил сообщение: ${event.data}`);
});

worker.postMessage('Привет, воркер!');

// Содержимое worker.js
self.addEventListener('message', (event) => {
  console.log(`Воркер получил сообщение: ${event.data}`);
  self.postMessage('Привет, родитель!');
});

// В этом примере родительский скрипт создает воркер и подписывается на событие message.
// Воркер также подписывается на это же событие и отправляет сообщение обратно родительскому скрипту.
// Поскольку оба скрипта подписаны на это событие, они могут общаться между собой,
// используя postMessage().

/*********************************************************************************************/

// использования метода terminate():

// Создание нового Web Worker
const worker2 = new Worker('worker.js');

// Отправка сообщения в Web Worker
worker2.postMessage('start');

// Прием сообщения из Web Worker
worker2.onmessage = function (event) {
  console.log('Результат работы: ' + event.data);

  // Завершение работы Web Worker
  worker2.terminate();
};

// Если не вызвать метод terminate(), то воркер будет продолжать работу,
// даже если его основной поток был завершен.
// Это может привести к утечке ресурсов и другим проблемам.
// Поэтому рекомендуется всегда вызывать terminate() при завершении работы с воркером.
